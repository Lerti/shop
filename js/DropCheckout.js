class DropCheckout {
    constructor(source, container = '#dropInformation') {
        this.source = source;
        this.container = container;
        this.informationItems = []; // Массив с пунктами списка
        this._init(this.source);
    }

    _init(source) {
        fetch(source)
            .then(result => result.json())
            .then(data => {
                for (let item of data.contents) {
                    this.informationItems.push(item);
                    this._renderItem(item);
                }
            })

    }

    _renderItem(item) {
        let $dropDiv = $('<div>', {
            class: 'drop'
        });
        let $informationWriteDiv = $('<div>', {
            class: item.divClass
        });
        let $informationLeftDiv = $('<div>', {
            class: 'information-left'
        });
        let $informationRightDiv = $('<div>', {
            class: 'information-right'
        });
        let $rightTwoDiv = $('<div>', {
            class: 'right-two'
        });
        let $productBorderDiv = $('<div>', {
            class: 'product-details-border-1'
        });

        $dropDiv.appendTo($(this.container));

        $dropDiv.append($(`<h3 class = "information-name"><br>${item.name}<i class="${item.iClass}"></i><br><br></h3>`));
        $informationWriteDiv.appendTo($($dropDiv));
        $informationLeftDiv.appendTo($($informationWriteDiv));
        $informationLeftDiv.append($(`<br><p class="name-types">Check as&nbsp;a&nbsp;guest or&nbsp;register</p>
            <p class="name-explain">Register with&nbsp;us for future convenience</p>
            <br><br>
            <p class="name-types"><input name="role" type="radio" value="guest"> checkout as&nbsp;guest</p>
            <p class="name-types"><input name="role" type="radio" value="register" checked="checked"> register</p>
            <br><br>
            <p class="name-types">register and save time!</p>
            <p class="name-explain">Register with&nbsp;us for future convenience</p>
            <br><br>
            <p class="name-explain"><i class="fas fa-angle-double-right"></i>Fast and easy checkout</p>
            <p class="name-explain"><i class="fas fa-angle-double-right"></i>Easy access to&nbsp;your order history and
                status</p>
            <a class="button-white information-write__button-white" href="product.html">Continue</a>`));

        $informationRightDiv.appendTo($($informationWriteDiv));
        $informationRightDiv.append($(`<br><br><br>
            <p class="name-types">Already registed?</p>
            <p class="name-explain">Please log in&nbsp;below</p>
            <br><br>
            <p class="name-types">EMAIL&nbsp;ADDRESS
                <sup class="note">*</sup></p>
            <input class="information-writing" type="email">
            <br><br>
            <p class="name-types">PASSWORD
                <sup class="note">*</sup></p>
            <input class="information-writing" type="password">
            <br><br>
            <p class="name-explain"><sup class="note">*</sup>
                <span class="red">Required Files</span>
            </p>`));
        $rightTwoDiv.appendTo($($informationRightDiv));
        $rightTwoDiv.append($(`<a class="button-white information-write__button-white-right" href="#"> Log in</a>
                <p class="name-explain">
                    <br><br><br><br><br>
                    <span class="black-password"> Forgot Password ?</span>
                </p>`));
        $productBorderDiv.appendTo($(this.container));
    }

    showHideInformation(item) {
        if($(item.children[1]).hasClass('pink')) {
            $(item.children[1]).removeClass('pink');
            $(item.nextSibling).addClass('hidden');
        } else {
            $(item.children[1]).addClass('pink');
            $(item.nextSibling).removeClass('hidden');
        }
    }
}
