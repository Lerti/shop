class Feedback {
    constructor(source, container = '#feedback', form = '#myForm') {
        this.source = source;
        this.container = container;
        this.form = form;
        this.comments = []; // Все отзывы
        this.curID = 0;
        this.alertWord = 0; // Для того, чтобы alert "Вы должны заполнить оба поля" не выскакивал при добавллении нормлаьного 2 комментария
        this._init(this.source);
    }

    _init(source) {
        fetch(source)
            .then(result => result.json())
            .then(data => {
                this.curID = data.maxID;
                for (let comment of data.comments) {
                    this.comments.push(comment);
                    this._renderComment(comment);
                }
                this._initFormAdd();
            });
    }

    _initFormAdd() {
        $(this.form).slideToggle(200);
        let $add = $(`<button class="button-white">Добавить отзыв</button>`);
        $('#button-add').append($add);
        $add.click(() => {
            this.alertWord = 0;
            this._initForm();
        });
    }

    _initForm() {
        $(this.form).slideToggle(200);
        $(this.form).submit(e => {
            e.preventDefault();
            if(this.alertWord === 1){
                return;
            }
            else if (!$('#author').val() || !$('#text').val()) {
                alert('Вы должны заполнить оба поля.\n');
                return;
            }
            else if (!(/^[a-zа-яё]{1,30}$/i.test($('#author').val()))) {
                alert('Имя должно содержать только буквы.\n');
                return;
            }
            let comment = {
                id: ++this.curID,
                author: $('#author').val(),
                text: $('#text').val(),
                approved: false
            };
            $('#author').val('');
            $('#text').val('');
            this.comments.push(comment);
            this._renderComment(comment);
            $(this.form).slideToggle(200);
        });
    }

    _renderComment(comment) {
        this.alertWord = 1;
        let $wrapper = $('<div/>', {
            class: 'comment',
            'data-id': comment.id
        });
        let $author = $(`<h3 class="author">${comment.author}</h3>`);
        let $text = $(`<p class="text">${comment.text}</p>`);
        $wrapper.append($author);
        $wrapper.append($text);

        if (!comment.approved) {
            let $approve = $(`<button class="approve-btn">Одобрить</button>`);
            $wrapper.append($approve);
            $wrapper.addClass('not-approved');
            $approve.click(() => {
                this._approve(comment.id);
            });
            let $delBtn = $(`<button class="remove-btn">Удалить</button>`);
            $wrapper.append($delBtn);
            $delBtn.click(() => {
                this._remove(comment.id);
            });
        } else {
            $wrapper.addClass('approved');
        }
        $(this.container).append($wrapper);
    }

    _approve(id) {
        let find = this.comments.find(comment => comment.id === id);
        $(`.comment[data-id="${id}"]`)
            .addClass('approved')
            .removeClass('not-approved')
            .find('.approve-btn')
            .remove();
        $(`.comment[data-id="${id}"]`)
            .find('.remove-btn')
            .remove();
        find.approved = true;
    }

    _remove(id) {
        let find = this.comments.find(comment => comment.id === id);
        this.comments.splice(this.comments.indexOf(find), 1);
        $(`.comment[data-id="${id}"]`).remove();
    }
}