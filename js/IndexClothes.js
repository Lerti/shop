class IndexClothes {
    constructor(source, container = '#place-for-index-clothes') {
        this.source = source;
        this.container = container;
        this.indexItems = [];
        //this.curID = 0;
        this._init(this.source);
    }

    _renderItem(product) {
        let $itemProductDiv = $('<div>', {
            class: 'item-product',
        });
        let $productA = $('<a>', {
            class: 'product'
        });
        let $productTextDiv = $('<div>', {
            class: 'product-text'
        });
        let $addFlexDiv = $('<div>', {
            class: 'add-flex'
        });
        let $addToCartBtn = $('<button>', {
            class: 'add-to-cart',

            'data-id': product.id_product,
            'data-name': product.product_name,
            'data-image': product.product_picture,
            'data-price': product.price,
            'data-color': product.product_color,
            'data-size': product.product_size,
        });

        $itemProductDiv.appendTo($(this.container));
        $productA.appendTo($($itemProductDiv));
        $productA.append($(`<img class="product-img" src="${product.product_picture}" alt="${product.product_alt}">`));
        $productTextDiv.appendTo($($productA));4
        $productTextDiv.append($(`<p class="name-product">${product.product_name}</p><p class="price-product">${product.price} .00 $</p>`));
        $addFlexDiv.appendTo($($itemProductDiv));
        $addToCartBtn.appendTo($($addFlexDiv));

        $addToCartBtn.append($(`<img src="img/add_to_cart.svg" alt="Add to cart"><p>Add to Cart</p>`));
    }

    _init(source) {
        fetch(source)
            .then(result => result.json())
            .then(data => {
                for (let product of data.contents) {
                    this.indexItems.push(product);
                    this._renderItem(product);
                }
            })
    }
}