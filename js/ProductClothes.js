class ProductClothes {
    constructor(source, container = '#place-for-product-clothes') {
        this.source = source;
        this.container = container;
        this.indexItems = [];
        //this.curID = 0;
        this._init(this.source);
    }

    _renderItem(product) {
        let $itemProductDiv = $('<div>', {
            class: 'item-product',
        });
        let $productA = $('<a>', {
            class: 'product'
        });
        let $productTextDiv = $('<div>', {
            class: 'product-text'
        });
        let $addFlexDiv = $('<div>', {
            class: 'add-flex'
        });
        let $columnsDiv = $('<div>', {
            class: 'columns'
        });
        let $bothDiv = $('<div>', {
            class: 'both'
        });
        let $addToCartBtn = $('<button>', {
            class: 'add-to-cart',

            'data-id': product.id_product,
            'data-name': product.product_name,
            'data-image': product.product_picture,
            'data-price': product.price,
            'data-color': product.product_color,
            'data-size': product.product_size,
        });

        $itemProductDiv.appendTo($(this.container));
        $productA.appendTo($($itemProductDiv));
        $productA.append($(`<img class="product-img" src="${product.product_picture}" alt="${product.product_alt}">`));
        $productTextDiv.appendTo($($productA));4
        $productTextDiv.append($(`<p class="name-product">${product.product_name}</p><p class="price-product">${product.price} .00 $</p>`));
        $addFlexDiv.appendTo($($itemProductDiv));
        $columnsDiv.appendTo($($addFlexDiv));
        $addToCartBtn.appendTo($($columnsDiv));
        $addToCartBtn.append($(`<img src="img/add_to_cart.svg" alt="Add to cart"><p>Add to Cart</p>`));
        $bothDiv.appendTo($($columnsDiv));
        $bothDiv.append($(`<br><button class="circle-like"> <img src="img/circle.svg" alt="Circle"></button>
<button class="circle-like"> <img src="img/like.svg" alt="LIke"></button>`));
    }

    _init(source) {
        fetch(source)
            .then(result => result.json())
            .then(data => {
                for (let product of data.contents) {
                    this.indexItems.push(product);
                    this._renderItem(product);
                }
            })
    }

    showDropSortNumber () {
        let number = '#drop-sort-number';
        let button = '#drop-sort-number-i';
        if($(number).hasClass("hidden")) {
            $(number).removeClass("hidden");
            $(button).addClass("pink");
            $(button).removeClass("grey");
        } else {
            $(number).addClass("hidden");
            $(button).removeClass("pink");
            $(button).addClass("grey");
        }
    }
}